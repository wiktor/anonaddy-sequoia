# AnonAddy Sequoia PHP bindings

This crate only works with PHP 8 due to the bindings library [ext-php-rs](https://github.com/davidcole1340/ext-php-rs).

Build with:

```bash
cargo build --release
```

Run with:

```bash
php -dextension=$(pwd)/target/release/libanonaddy_sequoia.so -a
```

## Available functions

### anonaddy_sequoia_encrypt

Encrypts a message and signs it:

```php
$signing_cert = file_get_contents("signing-key.asc");
$recipient_cert = file_get_contents("wiktor.asc");
echo anonaddy_sequoia_encrypt($signing_cert, $recipient_cert, "Hello Bruno!");
```

### anonaddy_sequoia_merge

Merges public information from a certificate:

```php
$current_cert = file_get_contents("wiktor-expired.asc");
$new_cert = file_get_contents("wiktor-fresh.asc");
echo anonaddy_sequoia_merge($current_cert, $new_cert);
```

### anonaddy_sequoia_minimize

Minimizes the certificate leaving only data necessary for `anonaddy_sequoia_encrypt`.

It is advisable to minimize cert that has been merged by `anonaddy_sequoia_merge` to
remove unnecessary data.

```php
$full = file_get_contents("wiktor-full.asc");
echo strlen($full);
# 195709
$min = anonaddy_sequoia_minimize($full);
echo strlen($min);
# 9863 
```
