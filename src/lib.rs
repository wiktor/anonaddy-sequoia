use ext_php_rs::{
    info_table_end, info_table_row, info_table_start,
    php::{
        args::{Arg, ArgParser},
        enums::DataType,
        execution_data::ExecutionData,
        function::FunctionBuilder,
        module::{ModuleBuilder, ModuleEntry},
        types::{zval::Zval},
    },
};

use std::{io::Write, time::SystemTime};

use anyhow::Context;

use sequoia_openpgp as openpgp;

use openpgp::{Cert, serialize::{SerializeInto, stream::{Armorer, Signer}}};
use openpgp::types::KeyFlags;
use openpgp::parse::Parse;
use openpgp::serialize::stream::{
    Message, LiteralWriter, Encryptor,
};
use openpgp::policy::StandardPolicy as P;

#[no_mangle]
pub extern "C" fn php_module_info(_module: *mut ModuleEntry) {
    info_table_start!();
    info_table_row!("anonaddy sequoia extension", "enabled");
    info_table_end!();
}

#[no_mangle]
pub extern "C" fn get_module() -> *mut ext_php_rs::php::module::ModuleEntry {
    let sequoia_encrypt_fn = FunctionBuilder::new("anonaddy_sequoia_encrypt", sequoia_encrypt)
        .arg(Arg::new("signing_cert", DataType::String))
        .arg(Arg::new("content", DataType::String))
        .arg(Arg::new("recipient_cert", DataType::String))
        .returns(DataType::String, false, false)
        .build();

    let sequoia_merge_fn = FunctionBuilder::new("anonaddy_sequoia_merge", sequoia_merge)
        .arg(Arg::new("existing_cert", DataType::String))
        .arg(Arg::new("new_cert", DataType::String))
        .returns(DataType::String, false, false)
        .build();

    let sequoia_minimize_fn = FunctionBuilder::new("anonaddy_sequoia_minimize", sequoia_minimize)
        .arg(Arg::new("cert", DataType::String))
        .returns(DataType::String, false, false)
        .build();

    ModuleBuilder::new("anonaddy-sequoia", "0.1.0")
        .info_function(php_module_info)
        .function(sequoia_encrypt_fn)
        .function(sequoia_merge_fn)
        .function(sequoia_minimize_fn)
        .build()
        .into_raw()
}

#[no_mangle]
pub extern "C" fn sequoia_encrypt(execute_data: &mut ExecutionData, _retval: &mut Zval) {
    let mut signing_cert_zval = Arg::new("signing_cert_zval", DataType::String);
    let mut recipient_cert_zval = Arg::new("recipient_cert_zval", DataType::String);
    let mut content_zval = Arg::new("content_zval", DataType::String);

    let result = ArgParser::new(execute_data)
        .arg(&mut signing_cert_zval)
        .arg(&mut recipient_cert_zval)
        .arg(&mut content_zval)
        .parse();

    if result.is_err() {
        return;
    }

    let signing_cert = signing_cert_zval.val::<String>().unwrap();
    let content = content_zval.val::<String>().unwrap();
    let recipient_cert = recipient_cert_zval.val::<String>().unwrap();

    let result = encrypt_for(signing_cert, recipient_cert, content).unwrap_or(String::from("something bad happened"));

    _retval.set_string(result);
}


fn encrypt_for(signing_cert: String, recipient_cert: String, content: String) -> openpgp::Result<String> {
    let p = &P::new();

    let mode = KeyFlags::empty().set_storage_encryption().set_transport_encryption();

    let signing_cert = Cert::from_bytes(&signing_cert)?
    .keys().unencrypted_secret()
    .with_policy(p, None).alive().revoked(false).for_signing()
    .nth(0).unwrap().key().clone().into_keypair()?;

    let certs: Vec<openpgp::Cert> = vec![Cert::from_bytes(&recipient_cert)?];

    let mut recipients = Vec::new();
    for cert in certs.iter() {
        let mut found_one = false;
        for key in cert.keys().with_policy(p, None)
            .alive().revoked(false).key_flags(&mode)
        {
            recipients.push(key);
            found_one = true;
        }

        if ! found_one {
            return Err(anyhow::anyhow!("No suitable encryption subkey for {}",
                                       cert));
        }
    }

    let mut sink = vec![];

    let message = Message::new(&mut sink);

    let message = Armorer::new(message).build()?;

    let message = Encryptor::for_recipients(message, recipients)
        .build().context("Failed to create encryptor")?;

    let message = Signer::new(message, signing_cert).build()?;

    let mut message = LiteralWriter::new(message).build()
        .context("Failed to create literal writer")?;

    message.write_all(content.as_bytes())?;

    message.finalize()?;

    Ok(String::from_utf8(sink)?)
}

#[no_mangle]
pub extern "C" fn sequoia_merge(execute_data: &mut ExecutionData, _retval: &mut Zval) {
    let mut existing_cert = Arg::new("existing_cert", DataType::String);
    let mut new_cert = Arg::new("new_cert", DataType::String);

    let result = ArgParser::new(execute_data)
        .arg(&mut existing_cert)
        .arg(&mut new_cert)
        .parse();

    if result.is_err() {
        return;
    }

    let existing_cert = existing_cert.val::<String>().unwrap();
    let new_cert = new_cert.val::<String>().unwrap();

    let merged_cert = merge_certs(existing_cert, new_cert).unwrap_or(String::from("something bad happened"));

    _retval.set_string(merged_cert);
}

fn merge_certs(existing_cert: String, new_cert: String) -> openpgp::Result<String> {
    let existing_cert = Cert::from_bytes(&existing_cert)?;
    let new_cert = Cert::from_bytes(&new_cert)?;

    let merged_cert = existing_cert.merge_public(new_cert)?;

    let armored = merged_cert.armored().to_vec()?;

    Ok(String::from_utf8(armored)?)
}

#[no_mangle]
pub extern "C" fn sequoia_minimize(execute_data: &mut ExecutionData, _retval: &mut Zval) {
    let mut cert = Arg::new("cert", DataType::String);

    let result = ArgParser::new(execute_data)
        .arg(&mut cert)
        .parse();

    if result.is_err() {
        return;
    }

    //php_parse_parameters!(&mut cert);

    let cert = cert.val::<String>().unwrap();

    let cert = minimize_cert(&cert).unwrap_or(String::from("something bad happened"));

    _retval.set_string(cert);
}

fn minimize_cert(cert: &String) -> openpgp::Result<String> {
    use std::convert::TryFrom;

    let ref policy = P::new();

    let cert = Cert::from_bytes(&cert)?;
    let cert = cert.with_policy(policy, None)?;

    let mut acc = Vec::new();

    let c = cert.primary_key();
    acc.push(c.key().clone().into());

    for s in c.self_signatures()   { acc.push(s.clone().into()) }
    for s in c.self_revocations()  { acc.push(s.clone().into()) }

    for c in cert.userids() {
        acc.push(c.userid().clone().into());
        for s in c.self_signatures()   { acc.push(s.clone().into()) }
        for s in c.self_revocations()  { acc.push(s.clone().into()) }
    }

    let flags = KeyFlags::empty().set_storage_encryption().set_transport_encryption();

    for c in cert.keys().subkeys().key_flags(flags).revoked(false) {
        if c.key_expiration_time().map(|time| time > SystemTime::now()).unwrap_or(true) {
            acc.push(c.key().clone().into());
            for s in c.self_signatures()   { acc.push(s.clone().into()) }
            for s in c.self_revocations()  { acc.push(s.clone().into()) }
        }
    }

    let filtered_cert = Cert::try_from(acc)?;

    let armored = filtered_cert.armored().to_vec()?;

    Ok(String::from_utf8(armored)?)
}
